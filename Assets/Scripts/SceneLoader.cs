﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] float LoadDelay = 3f;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadSplash", LoadDelay);
    }
       
    void LoadSplash()
    {
        SceneManager.LoadScene(1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerValuesBackup : MonoBehaviour
{
    [Tooltip("In ms^-1")] [SerializeField] float Speed = 40f;
    [Tooltip("In m")] [SerializeField] float xRange = 20f;
    [Tooltip("In m")] [SerializeField] float yRange = 12f;

    [SerializeField] float positionPitchFactor = -1f;
    [SerializeField] float controlPitchFactor = -30f;

    [SerializeField] float positionYawFactor = -1f;
    [SerializeField] float controlRollFactor = -30f;

}
